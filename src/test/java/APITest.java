import com.jayway.restassured.response.Response;
import com.jayway.restassured.response.ValidatableResponse;
import org.junit.Test;
import org.junit.platform.commons.annotation.Testable;

import static com.jayway.restassured.RestAssured.*;
import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;
import static org.hamcrest.CoreMatchers.equalTo;

public class APITest extends Constants{
//dd

    @Test

    public void checkTotalElements(){
        ValidatableResponse total = when().

                get(Constants.globalUrl +"/users?page=2").
                then().
                statusCode(200).
                assertThat().body("total", equalTo(12), "page", equalTo(2));
        Response response = get(Constants.globalUrl + "/users?page=2");
        LOGGER.info("Response: " + response.getBody().asString() + "\n");
        System.out.println("Test checkTotalElements executed");
    }
    @Test
    public void createNewPerson () {
        given().contentType("application/json").body("{\n" +
                "    \"name\": \"morpheus\",\n" +
                "    \"job\": \"leader\"\n" +
                "}").
        when().
                post(Constants.globalUrl + "/api/users").
        then().
                statusCode(201).
                assertThat().
                body("job", equalTo("leader"), "name", equalTo("morpheus"));
        Response response = get("https://reqres.in/api/users/2");
        LOGGER.info("New user created:" + "\n" + response.getBody().asString() + "\n");
        System.out.println("Test createNewPerson executed");
    }
}
